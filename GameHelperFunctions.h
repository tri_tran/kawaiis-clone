#ifndef GAMEHELPERFUNCTIONS_H
#define GAMEHELPERFUNCTIONS_H

#include <SFML/Graphics.hpp>
#include <sstream>
#include <iomanip>

sf::Vector2f center(const sf::Vector2f&, const sf::Vector2u&);
std::string rightJustify(int, int);
sf::String trim(const sf::String&);

#endif // GAMEHELPERFUNCTIONS_H
