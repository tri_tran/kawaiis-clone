#ifndef ZIPPACK_H
#define ZIPPACK_H

#include <zip.h>
#include <vector>
#include <string>

class FileContent;

class ZipPack
{
public:
    ZipPack(const char* = NULL);
    ~ZipPack();
    zip* zPtr()const { return z; }
    operator bool()const { return z != NULL; }
    bool open(const char*);
    void close();
    size_t fcount()const;
    std::vector<std::string> getAllFileNames()const;
private:
    zip* z;
};

#endif // ZIPPACK_H
