# Kawaii's Clone
Find matches tiles that are connected by at most 3 lines.  
- F1 to show hint   
- F5 to shuffle  
- Right click to deselect  
- 7.5 min per game  

### Screenshots

![play](screenshots/play.png)

### Resource's requirements
- assets/assets.zip (textures, images, fonts, sounds, and musics)

### Dependencies
- SFML v2.3.2 (zlib/png license)  
- OpenAL v1.17.1 (LGPL)  
- libzip v1.0.1 (Libzip license)  
- Boost v1.60.0 (Boost license)  
- TGUI 0.7-dev (TGUI license)  
- sftools/Chronometer

### Other licenses
- Falling Sky font by Cannot Into Space Fonts (SIL Open Font License)  

### To Do List

### Wish list
- New save file (sqlite, json, ...?)  
- Customize main menu GUI  
- Customize high score GUI  

### Documentation
- FSM diagram using http://madebyevan.com/fsm/  

## Notes
1. Boost: need Boost's `random_device`. Build only `random` library:  
```
boostrap mingw  
b2 release toolset=gcc random  
```
- Linker settings: `-lboost_random-<compiler>-<boostversion>`  
- Dependencies: copy *libboost_random-<compiler>-<boostversion>.dll* and *libboost_system-<compiler>-<boostversion>.dll* to your executable folder. 