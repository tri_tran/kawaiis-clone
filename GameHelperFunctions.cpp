#include "GameHelperFunctions.h"

sf::Vector2f center(const sf::Vector2f& s, const sf::Vector2u& container)
{
    return {(container.x - s.x)/2, (container.y - s.y)/2};
}

std::string rightJustify(int n, int width)
{
    std::ostringstream oss;
    oss << std::right << std::setw(width) << n;
    return oss.str();
}

sf::String trim(const sf::String& in)
{
    auto s = in.toUtf32();
    int i = 0, j = s.size()-1;
    while (s[i]==' ' || s[i]==0xA0) ++i;
    while (s[j]==' ' || s[j]==0xA0) --j;
    return s.substr(i, j-i+1);
}
