#ifndef SPLASHSCREEN_H
#define SPLASHSCREEN_H

#include <SFML/Graphics.hpp>
#ifndef NO_SPLASH_SCREEN
#include "TransparentWindows.h"
#endif // NO_SPLASH_SCREEN
#include "ResourceManager.h"

extern const sf::Time SPLASH_TIME;
extern const std::string RES_PATH;

class SplashScreen
{
public:
    static void run(sf::RenderWindow&, ResourceManager&);
};

#endif // SPLASHSCREEN_H
