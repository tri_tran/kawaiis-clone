#include "Grid.h"

namespace kawaii
{
const int Grid::EMPTY;

Grid::Grid(int w, int h, int pairs)
:
#ifndef _MSC_VER
	rng(boost::random_device{}()),
#else
	rng(std::random_device{}()),
#endif
  data(h, std::vector<int>(w, EMPTY)), w(w), h(h),
  groupSize(2*pairs), totalPairs(w*h/pairs),
  work(h+2, std::vector<PathfindingCell>(w+2)),
  r1(-1), c1(-1), r2(-1), c2(-1)
{
    reset();
}

void Grid::reset(int startId)
{
    totalPairs = w*h/groupSize*2;
    clearState();
    // refill data
    int count = groupSize + startId*groupSize;  //cell starts from 1
    for (auto& row : data)
        for (int& cell : row)
            cell = count++ / groupSize;
    shuffle();
    // Make sure that there is at least a match
    findRandomMatch();
    while (!hasHint())
    {
        shuffle();
        findRandomMatch();
    }
}

void Grid::resize(int w, int h)
{
    this->w = w;
    this->h = h;
    data.resize(h, std::vector<int>(w, EMPTY));
}

bool Grid::hasTile(const Coord& coord)const
{
    return data[coord.r][coord.c] != EMPTY;
}

void Grid::clearState()
{
    path.clear();
    deselectLast();
    deselectLast();
    hint.first = hint.second = Coord();
    hiddenHint.first = hiddenHint.second = Coord();
}

void Grid::shuffle()
{
    if (selected() == 1) deselectLast();
    auto gRow = this->toGridRow();
    std::shuffle(gRow.begin(), gRow.end(), rng);
    this->parseToGrid(gRow);
    this->clearState();
}

// Convert 2D grid to 1D row
std::vector<int> Grid::toGridRow()const
{
    std::vector<int> gRow(w*h, EMPTY);
    int count = 0;
    for (auto const& row : data)
        for (int cell : row)
            if (cell) gRow[count++] = cell;
    gRow.resize(count);
    return gRow;
}

// Convert 1D row to 2D grid
void Grid::parseToGrid(const std::vector<int>& gRow)
{
    int i = 0;
    for (auto& row : data)
        for (int& cell : row)
            if (cell) cell = gRow[i++];
}

void Grid::select(int r, int c)
{
    if (r == - 1 || c == -1 || !data[r][c]) //empty select
        return;
    if (selections.first.r == -1 //first select or repeat first select
        || (selections.first.r == r && selections.first.c == c))
        selections.first.r = r, selections.first.c = c;
    else
        selections.second.r = r, selections.second.c = c;
}

void Grid::deselectLast()
{
    if (selections.second.r != -1)
        selections.second.r = selections.second.c = -1;
    else
        selections.first.r = selections.first.c = -1;
}

void Grid::findPath()
{
    r1 = selections.first.r, c1 = selections.first.c;
    r2 = selections.second.r, c2 = selections.second.c;
    this->findConnectedPath();
}

void Grid::findConnectedPath()
{
    // Reset `work` and `path`
    for (auto& row : work)
        std::fill(row.begin(), row.end(), PathfindingCell());
    path.clear();

    // Init queue, `work`, and outDir
    std::queue<Coord> q; //r,c of `work`
    q.emplace(r1+1, c1+1);
    work[r1+1][c1+1] = PathfindingCell(B_DIR, 3);
    std::string outDirs;

    while (!q.empty())
    {
        auto coord = q.front();
        q.pop();
        auto pfCell = work[coord.r][coord.c];

        int nextTurns = pfCell.turns - 1;
        if (nextTurns == -1) continue;
        if (pfCell.direction & H_DIR)
        {
            exploreLeft(coord, nextTurns, outDirs, q);
            exploreRight(coord, nextTurns, outDirs, q);
        }
        if (pfCell.direction & V_DIR)
        {
            exploreTop(coord, nextTurns, outDirs, q);
            exploreBottom(coord, nextTurns, outDirs, q);
        }
    }
    if (!outDirs.empty())
    {
        path.clear();
        path = buildPath(outDirs[0]);
        for (size_t i = 1; i < outDirs.size(); ++i)
        {
            auto p = buildPath(outDirs[i]);
            if (getPathLength(p) < getPathLength(path)) path = p;
        }
    }
}

int Grid::getPathLength(const std::vector<Coord>& p)
{
    int len = 0;
    for (size_t i = 1; i < p.size(); ++i)
        len += std::abs(p[i].r - p[i-1].r) + std::abs(p[i].c - p[i-1].c);
    return len;
}

Coord Grid::getDirVect(char dir)
{
    switch (dir)
    {
    case 'L': return Coord( 0, -1);
    case 'R': return Coord( 0,  1);
    case 'T': return Coord(-1,  0);
    default : return Coord( 1,  0);
    }
}

std::vector<Coord> Grid::buildPath(char outDir)
{
    std::vector<Coord> result;
    int r = r2, c = c2;
    result.emplace_back(r, c);
    int turns = work[r+1][c+1].turns;
    while (r != r1 || c != c1)
    {
        auto dir = getDirVect(outDir);
        while (work[r+1][c+1].turns == turns)
            r += dir.r, c += dir.c;
        result.emplace_back(r, c);
        turns = work[r+1][c+1].turns;
        if (outDir=='L' || outDir=='R') outDir = (r > r1)["BT"];
        else                            outDir = (c > c1)["RL"];
    }
    return result;
}

bool Grid::hasPath()const
{
    return !path.empty();
}

void Grid::removePair()
{
    if (selections.first.r != -1 && selections.first.c != -1
        && selections.second.r != -1  && selections.second.c != -1
        && data[selections.first.r][selections.first.c]
           == data[selections.second.r][selections.second.c]
        && hasPath())
    {
        if (selections.first.r != -1)
            data[selections.first.r][selections.first.c] = EMPTY;
        if (selections.second.r != -1)
            data[selections.second.r][selections.second.c] = EMPTY;
        totalPairs--;
        shift();
        clearState();
    }
}

int Grid::selected()const
{
    if (selections.second.r != -1) return 2;
    if (selections.first.r != -1) return 1;
    return 0;
}

bool Grid::match()const
{
    if (selected() < 2) return false;
    return data[selections.first.r][selections.first.c]
           == data[selections.second.r][selections.second.c];
}

void Grid::showHint()
{
    hint = hiddenHint;
}

void Grid::findRandomMatch()
{
    // Clear result
    hiddenHint.first = hiddenHint.second = Coord();

    // Build index list by id
    std::vector<std::vector<Coord>> indexById(w*h/groupSize + 1);
    for (int r = h; r-->0; ) for (int c = w; c-->0; )
        if (data[r][c])
            indexById[data[r][c]%indexById.size()].emplace_back(r, c);

    // Randomize id groups
    std::shuffle(indexById.begin(), indexById.end(), rng);

    // Find connected pair
    for (auto& group : indexById)
    {
        // Also randomize orders in group
        std::shuffle(group.begin(), group.end(), rng);
        // Find connected pair in group
        for (int i = group.size(); i-->1; ) for (int j = i; j-->0; )
        {
            Coord first = group[i], second = group[j];
            r1 = first.r, c1 = first.c;
            r2 = second.r, c2 = second.c;
            this->findConnectedPath();
            if (this->hasPath())
            {
                hiddenHint.first = first;
                hiddenHint.second = second;
                path.clear();
                return;
            }
        }
    }
}

bool Grid::inRange(int r, int c)const
{
    return r >= 0 && r < h && c >= 0 && c < w;
}

void Grid::checkDestination(int r, int c, int nextTurns,
                                    std::string& outDirs, char d)
{
    if (r == r2 && c == c2)
    {
        if (nextTurns > work[r2+1][c2+1].turns)
        {
            work[r2+1][c2+1].turns = nextTurns;
            outDirs = std::string(1, d);
        }
        else if (nextTurns == work[r2+1][c2+1].turns)
            outDirs += d;
    }
}

void Grid::explore(int r, int c, int nextTurns,
                           int removeDir, std::queue<Coord>& q)
{
    work[r][c].turns = nextTurns;
    work[r][c].direction &= ~removeDir;
    if (work[r][c].direction)
        q.emplace(r, c);
}

void Grid::exploreTop(const Coord& coord, int nextTurns,
                              std::string& outDirs, std::queue<Coord>& q)
{
    for (int r = coord.r; r-->0; )
    {
        if (inRange(r-1, coord.c-1) && data[r-1][coord.c-1])
        {
            checkDestination(r-1, coord.c-1, nextTurns, outDirs, 'B');
            break; //hit block, stop explore
        }
        if (nextTurns >= work[r][coord.c].turns)
            explore(r, coord.c, nextTurns, V_DIR, q);
        else break;
    }
}

void Grid::exploreBottom(const Coord& coord, int nextTurns,
                                 std::string& outDirs, std::queue<Coord>& q)
{
    for (int r = coord.r; ++r < h+2; )
    {
        if (inRange(r-1, coord.c-1) && data[r-1][coord.c-1])
        {
            checkDestination(r-1, coord.c-1, nextTurns, outDirs, 'T');
            break;
        }
        if (nextTurns >= work[r][coord.c].turns)
            explore(r, coord.c, nextTurns, V_DIR, q);
        else break;
    }
}

void Grid::exploreLeft(const Coord& coord, int nextTurns,
                               std::string& outDirs, std::queue<Coord>& q)
{
    for (int c = coord.c; c-->0; )
    {
        if (inRange(coord.r-1, c-1) && data[coord.r-1][c-1])
        {
            checkDestination(coord.r-1, c-1, nextTurns, outDirs, 'R');
            break;
        }
        if (nextTurns >= work[coord.r][c].turns)
            explore(coord.r, c, nextTurns, H_DIR, q);
        else break;
    }
}

void Grid::exploreRight(const Coord& coord, int nextTurns,
                                std::string& outDirs, std::queue<Coord>& q)
{
    for (int c = coord.c; ++c < w+2; )
    {
        if (inRange(coord.r-1, c-1) && data[coord.r-1][c-1])
        {
            checkDestination(coord.r-1, c-1, nextTurns, outDirs, 'L');
            break;
        }
        if (nextTurns >= work[coord.r][c].turns)
            explore(coord.r, c, nextTurns, H_DIR, q);
        else break;
    }
}

void Grid::swapTile(int coordA_r, int coordA_c,
                            int coordB_r, int coordB_c)
{
    std::swap(data[coordA_r][coordA_c], data[coordB_r][coordB_c]);
}


void Grid::shift()
{
    shiftFunc(*this);
}

void Grid::setShiftCallback(ShiftCallbackFunc f)
{
    shiftFunc = f;
}

void Grid::defaultShift(Grid& grid)
{
    // default shift - do nothing
}

void Grid::shiftLeft(Grid& grid)
{
    int affectedRows[] = { grid.getSelections().first.r,
                           grid.getSelections().second.r };
    for (int i : affectedRows)
    {
        int w = 0;
        for (w = 0; w < grid.cols() && grid[i][w]; ++w);
        for (int j = w+1; j < grid.cols(); ++j)
            if (grid[i][j]) grid.swapTile(i, w++, i, j);
    }
}

void Grid::shiftRight(Grid& grid)
{
    int affectedRows[] = { grid.getSelections().first.r,
                           grid.getSelections().second.r };
    for (int i : affectedRows)
    {
        int w = 0;
        for (w = grid.cols(); w-->0 && grid[i][w]; );
        for (int j = w; j-->0; )
            if (grid[i][j]) grid.swapTile(i, w--, i, j);
    }
}

void Grid::shiftUp(Grid& grid)
{
    int affectedCols[] = { grid.getSelections().first.c,
                           grid.getSelections().second.c };
    for (int j : affectedCols)
    {
        int w = 0;
        for (w = 0; w < grid.rows() && grid[w][j]; ++w);
        for (int i = w+1; i < grid.rows(); ++i)
            if (grid[i][j]) grid.swapTile(w++, j, i, j);
    }
}

void Grid::shiftDown(Grid& grid)
{
    int affectedCols[] = { grid.getSelections().first.c,
                           grid.getSelections().second.c };
    for (int j : affectedCols)
    {
        int w = 0;
        for (w = grid.rows(); w-->0 && grid[w][j]; );
        for (int i = w; i-->0; )
            if (grid[i][j]) grid.swapTile(w--, j, i, j);
    }
}

void Grid::shiftOutVertical(Grid& grid)
{
    Coord affectedCells[] = { grid.getSelections().first,
                              grid.getSelections().second };
    for (auto cell : affectedCells)
    {
        int w = 0;
        int j = cell.c;
        if (cell.r < grid.rows()/2)
        {   //shift up
            for (w = 0; w < grid.rows()/2 && grid[w][j]; ++w);
            for (int i = w+1; i < grid.rows()/2; ++i)
                if (grid[i][j]) grid.swapTile(w++, j, i, j);
        }
        else
        {   //shift down
            for (w = grid.rows(); w-->grid.rows()/2 && grid[w][j]; );
            for (int i = w; i-->grid.rows()/2; )
                if (grid[i][j]) grid.swapTile(w--, j, i, j);
        }
    }
}

void Grid::shiftOutHorizontal(Grid& grid)
{
    Coord affectedCells[] = { grid.getSelections().first,
                              grid.getSelections().second };
    for (auto cell : affectedCells)
    {
        int w = 0;
        int i = cell.r;
        if (cell.c < grid.cols()/2)
        {   //shift left
            for (w = 0; w < grid.cols()/2 && grid[i][w]; ++w);
            for (int j = w+1; j < grid.cols()/2; ++j)
                if (grid[i][j]) grid.swapTile(i, w++, i, j);
        }
        else
        {   //shift right
            for (w = grid.cols(); w-->grid.cols()/2 && grid[i][w]; );
            for (int j = w; j-->grid.cols()/2; )
                if (grid[i][j]) grid.swapTile(i, w--, i, j);
        }
    }
}

void Grid::shiftInVertical(Grid& grid)
{
    Coord affectedCells[] = { grid.getSelections().first,
                              grid.getSelections().second };
    for (auto cell : affectedCells)
    {
        int w = 0;
        int j = cell.c;
        if (cell.r < grid.rows()/2)
        {   //shift down
            for (w = grid.rows()/2; w-->0 && grid[w][j]; );
            for (int i = w; i-->0; )
                if (grid[i][j]) grid.swapTile(w--, j, i, j);
        }
        else
        {   //shift up
            for (w = grid.rows()/2; w < grid.rows() && grid[w][j]; ++w);
            for (int i = w+1; i < grid.rows(); ++i)
                if (grid[i][j]) grid.swapTile(w++, j, i, j);
        }
    }
}

void Grid::shiftInHorizontal(Grid& grid)
{
    Coord affectedCells[] = { grid.getSelections().first,
                              grid.getSelections().second };
    for (auto cell : affectedCells)
    {
        int w = 0;
        int i = cell.r;
        if (cell.c < grid.cols()/2)
        {   //shift right
            for (w = grid.cols()/2; w-->0 && grid[i][w]; );
            for (int j = w; j-->0; )
                if (grid[i][j]) grid.swapTile(i, w--, i, j);
        }
        else
        {   //shift left
            for (w = grid.cols()/2; w < grid.cols() && grid[i][w]; ++w);
            for (int j = w+1; j < grid.cols(); ++j)
                if (grid[i][j]) grid.swapTile(i, w++, i, j);
        }
    }
}

}
