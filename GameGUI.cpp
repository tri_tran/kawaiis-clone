#include "GameGUI.h"
#include "Game.h"

void GameGUI::loadMainMenuWidget()
{
    Game& game = Game::instance();
    // Setup menu title
    auto title = std::make_shared<tgui::Label>();
    title->setFont(game.font);
    title->setTextSize(48);
    title->setTextColor(sf::Color::Green);
    title->setText("Kawaii's Clone");
    title->setPosition(center(title->getSize(), game.window.getSize())
                       - sf::Vector2f(0, 225));
    add(title, "MainTitle");

    auto menuGrid = std::make_shared<tgui::Grid>();
    auto btnBorder = tgui::Borders(0, 2.5f, 0, 2.5f);

    auto newGameBtn = std::make_shared<tgui::Button>();
    newGameBtn->setSize(150, 30);
    newGameBtn->setText("New Game");
    menuGrid->addWidget(newGameBtn, 0, 0, btnBorder);

    auto hiScrBtn = tgui::Button::copy(newGameBtn);
    hiScrBtn->setText("High Scores");
    menuGrid->addWidget(hiScrBtn, 1, 0, btnBorder);

    auto exitBtn = tgui::Button::copy(newGameBtn);
    exitBtn->setText("Exit");
    menuGrid->addWidget(exitBtn, 2, 0, btnBorder);

    newGameBtn->connect("clicked", [](){ Game::instance().newGame(); });
    hiScrBtn->connect("clicked", [this](){
        hideMainMenuWidget();
        showHighScoreWidget(-1);
    });
    exitBtn->connect("clicked", [](){ Game::instance().window.close(); });

    menuGrid->setPosition(center(menuGrid->getSize(), game.window.getSize()));
    add(menuGrid, "MainMenu");
}

void GameGUI::hideMainMenuWidget()
{
    get<tgui::Grid>("MainMenu")->hide();
    get<tgui::Label>("MainTitle")->hide();
}

void GameGUI::showMainMenuWidget()
{
    get<tgui::Grid>("MainMenu")->showWithEffect(tgui::ShowAnimationType::Fade, FADE_IN_DELAY);
    get<tgui::Label>("MainTitle")->showWithEffect(tgui::ShowAnimationType::Fade, FADE_IN_DELAY);
}



void GameGUI::loadHighScoreWidget()
{
    Game& game = Game::instance();
    // Setup high score title
    auto hiScrTitle = std::make_shared<tgui::Label>();
    hiScrTitle->setFont(game.font);
    hiScrTitle->setTextSize(48);
    hiScrTitle->setTextColor(sf::Color::Green);
    hiScrTitle->setText("High Scores");
    hiScrTitle->setPosition(center(hiScrTitle->getSize(), game.window.getSize())
                            - sf::Vector2f(0, 225));
    add(hiScrTitle, "HighScoreTitle");

    // Setup high score table's  background
    auto hiScoreBg = std::make_shared<tgui::Picture>(game.hiScoreTex);
    hiScoreBg->setPosition(center(hiScoreBg->getSize(), game.window.getSize()));
    add(hiScoreBg, "HighScoreTableBackground");

    // Setup high scores table
    auto hiScoreGrid = std::make_shared<tgui::Grid>();
    auto rowBorder = tgui::Borders(8, 1, 9, 1);
    // Load label `themes`
    auto rankLabel = std::make_shared<tgui::Label>();
    rankLabel->setTextColor(sf::Color::Black);
    rankLabel->setSize(30, 30);
    auto nameLabel = tgui::Label::copy(rankLabel);
    nameLabel->setSize(245, 30);
    auto scoreLabel = tgui::Label::copy(rankLabel);
    scoreLabel->setSize(70, 30);
    // Load high scores from file
    if (!game.loadHighScores())
    {
        game.saveHighScores();
        game.loadHighScores();
    }

    // Populate high scores table
    for (int i = 0; i < 10; ++i)
    {
        auto rankLbl = tgui::Label::copy(rankLabel);
        rankLbl->setText(rightJustify(i+1, 2));
        hiScoreGrid->addWidget(rankLbl, i, 0, rowBorder);
        auto nameLbl = tgui::Label::copy(nameLabel);
        nameLbl->setText(game.highScores[i].name);
        nameLbl->setFont(game.font);
        hiScoreGrid->addWidget(nameLbl, i, 1, rowBorder, tgui::Grid::Alignment::Left);
        auto scoreLbl = tgui::Label::copy(scoreLabel);
        scoreLbl->setText(rightJustify(game.highScores[i].score, 5));
        hiScoreGrid->addWidget(scoreLbl, i, 2, rowBorder);
    }
    // Set position & add to gui
    hiScoreGrid->setPosition(hiScoreBg->getPosition() + sf::Vector2f(10, 4));
    add(hiScoreGrid, "HighScoreGrid");

    // Setup confirm button
    auto okBtn = std::make_shared<tgui::Button>();
    okBtn->setText("Ok");
    okBtn->setPosition(center(okBtn->getSize(), game.window.getSize()) + sf::Vector2f(0, 200));
    add(okBtn, "ConfirmHighScore");
    okBtn->hide();

    // Setup return button
    auto returnBtn = std::make_shared<tgui::Button>();
    returnBtn->setText("Return");
    returnBtn->setPosition(center(returnBtn->getSize(), game.window.getSize()) + sf::Vector2f(0, 200));
    returnBtn->connect("clicked", [](){ Game::instance().toMainMenuState(); });
    add(returnBtn, "ReturnToMainMenuButton");
}

void GameGUI::hideHighScoreWidget()
{
    get<tgui::Label>("HighScoreTitle")->hide();
    get<tgui::Picture>("HighScoreTableBackground")->hide();
    get<tgui::Grid>("HighScoreGrid")->hide();
    get<tgui::Button>("ConfirmHighScore")->hide();
    get<tgui::Button>("ReturnToMainMenuButton")->hide();
}

void GameGUI::showHighScoreWidget(int playerScore)
{
    Game& game = Game::instance();
    auto title = get<tgui::Label>("HighScoreTitle");
    auto tableBg = get<tgui::Picture>("HighScoreTableBackground");
    auto table = get<tgui::Grid>("HighScoreGrid");
    auto okBtn = get<tgui::Button>("ConfirmHighScore");
    auto returnBtn = get<tgui::Button>("ReturnToMainMenuButton");

    returnBtn->disconnectAll();
    if (playerScore == -1) //already in main menu, don't restart music
        returnBtn->connect("clicked", [](){ Game::instance().toMainMenuState(0); });
    else
        returnBtn->connect("clicked", [](){ Game::instance().toMainMenuState(); });

    title->showWithEffect(tgui::ShowAnimationType::Fade, FADE_IN_DELAY);
    tableBg->showWithEffect(tgui::ShowAnimationType::Fade, FADE_IN_DELAY);

    // If playerScore is not high score, just show the table 
    auto currentPlacePtr = std::find_if(game.highScores.begin(),
                                        game.highScores.end(),
        [playerScore](const HighScore& h){
            return h.score < playerScore;
        });
    if (currentPlacePtr == game.highScores.end())
    {
        table->showWithEffect(tgui::ShowAnimationType::Fade, FADE_IN_DELAY);
        returnBtn->showWithEffect(tgui::ShowAnimationType::Fade, FADE_IN_DELAY);
        return;
    }
    //else playerScore is high score
    // Add to highScores
    int cur = std::distance(game.highScores.begin(), currentPlacePtr);
    game.highScores.insert(currentPlacePtr, HighScore(playerScore));
    // Update cur row display
    auto nameLbl = std::dynamic_pointer_cast<tgui::Label>(table->getWidget(cur, 1));
    table->remove(nameLbl);
    auto playerNameEdit = std::make_shared<tgui::EditBox>();
    playerNameEdit->setSize(245, 23);
    playerNameEdit->setMaximumCharacters(MAX_PLAYER_NAME_LEN);
    playerNameEdit->setDefaultText("<Your name>");
    playerNameEdit->setFont(game.font);
    table->addWidget(playerNameEdit, cur, 1, tgui::Borders(8, 1, 9, 1),
                     tgui::Grid::Alignment::UpperLeft);
    auto scoreLbl = std::make_shared<tgui::Label>();
    scoreLbl->setTextColor(sf::Color::Black);
    scoreLbl->setSize(70, 30);
    scoreLbl->setText(rightJustify(game.highScores[cur].score, 5));
    table->addWidget(scoreLbl, cur, 2, tgui::Borders(8, 1, 9, 1));
    // Update other rows (below current place) display
    for (int i = cur+1; i < 10; ++i)
    {
        nameLbl = std::dynamic_pointer_cast<tgui::Label>(table->getWidget(i, 1));
        nameLbl->setText(game.highScores[i].name);
        scoreLbl = std::dynamic_pointer_cast<tgui::Label>(table->getWidget(i, 2));
        scoreLbl->setText(rightJustify(game.highScores[i].score, 5));
    }
    // Create confirm name function
    auto confirmNameFunc = [this, cur](){
        Game& game = Game::instance();
        auto table = get<tgui::Grid>("HighScoreGrid");
        auto inNameBox = std::dynamic_pointer_cast<tgui::EditBox>(table->getWidget(cur, 1));
        game.highScores[cur].name = trim(inNameBox->getText());
        inNameBox->setText(game.highScores[cur].name);
        if (!game.highScores[cur].name.getSize())
        {
            inNameBox->setDefaultText("<Name cannot be blank>");
            return;
        }
        game.saveHighScores();
        // Update cur row display
        table->remove(inNameBox);
        auto scoreLbl = std::make_shared<tgui::Label>();
        scoreLbl->setTextColor(sf::Color::Black);
        scoreLbl->setSize(70, 30);
        scoreLbl->setText(rightJustify(game.highScores[cur].score, 5));
        table->addWidget(scoreLbl, cur, 2, tgui::Borders(8, 1, 9, 1));
        auto nameLbl = tgui::Label::copy(scoreLbl);
        nameLbl->setSize(245, 30);
        nameLbl->setText(game.highScores[cur].name);
        table->addWidget(nameLbl, cur, 1, tgui::Borders(8, 1, 9, 1));

        get<tgui::Button>("ConfirmHighScore")->hide();
        get<tgui::Button>("ReturnToMainMenuButton")->show();
    };
    // Connect confirm button callback
    okBtn->disconnectAll("clicked");
    okBtn->connect("clicked", confirmNameFunc);
    playerNameEdit->connect("returnKeyPressed", confirmNameFunc);

    // Show table and confirm button
    table->showWithEffect(tgui::ShowAnimationType::Fade, FADE_IN_DELAY);
    okBtn->showWithEffect(tgui::ShowAnimationType::Fade, FADE_IN_DELAY);
}



void GameGUI::loadPauseMenuWidget()
{
    Game& game = Game::instance();
    // Setup pause game title
    auto title = std::make_shared<tgui::Label>();
    title->setFont(game.font);
    title->setTextSize(48);
    title->setTextColor(sf::Color::Green);
    title->setText("Game Paused");
    title->setPosition(center(title->getSize(), game.window.getSize()) - sf::Vector2f(0, 225));
    add(title, "GamePausedTitle");

    // Pause menu
    auto menuGrid = std::make_shared<tgui::Grid>();
    auto btnBorder = tgui::Borders(0, 2.5f, 0, 2.5f);

    auto resumeBtn = std::make_shared<tgui::Button>();
    resumeBtn->setSize(150, 30);
    resumeBtn->setText("Resume");
    menuGrid->addWidget(resumeBtn, 0, 0, btnBorder);

    auto toMainMenuBtn = tgui::Button::copy(resumeBtn);
    toMainMenuBtn->setText("To Main Menu");
    menuGrid->addWidget(toMainMenuBtn, 1, 0, btnBorder);

    resumeBtn->connect("clicked", [this](){ Game::instance().toMMFromPS(); });
    toMainMenuBtn->connect("clicked", [this](){
        hidePauseMenuWidget();
        showReturnMMConfirmWidget();
    });

    menuGrid->setPosition(center(menuGrid->getSize(), game.window.getSize()));
    add(menuGrid, "PauseMenu");

    // Confirm return to main menu widget
    auto mmTitle = tgui::Label::copy(title);
    mmTitle->setTextSize(30);
    mmTitle->setText("Game progress will be lost. Proceed?");
    mmTitle->setPosition(center(mmTitle->getSize(), game.window.getSize()) - sf::Vector2f(0, 100));
    add(mmTitle, "ReturnToMainMenuConfirmTitle");

    // Confirm buttons
    auto mmGrid = std::make_shared<tgui::Grid>();

    auto yesBtn = std::make_shared<tgui::Button>();
    yesBtn->setSize(100, 30);
    yesBtn->setText("Yes");
    mmGrid->addWidget(yesBtn, 0, 0, tgui::Borders(2.5f, 0, 2.5f, 0));

    auto noBtn = tgui::Button::copy(yesBtn);
    noBtn->setText("No");
    mmGrid->addWidget(noBtn, 0, 1, tgui::Borders(2.5f, 0, 2.5f, 0));

    yesBtn->connect("clicked", [](){ Game::instance().toMainMenuState(); });
    noBtn->connect("clicked", [this](){
        hideReturnMMConfirmWidget();
        showPauseMenuWidget();
        Game::instance().unpauseDelay.restart();
    });

    mmGrid->setPosition(center(mmGrid->getSize(), game.window.getSize()));
    add(mmGrid, "ReturnToMainMenuConfirmMenu");
}

void GameGUI::hidePauseMenuWidget()
{
    get<tgui::Grid>("PauseMenu")->hide();
    get<tgui::Label>("GamePausedTitle")->hide();
}

void GameGUI::showPauseMenuWidget()
{
    get<tgui::Grid>("PauseMenu")->showWithEffect(tgui::ShowAnimationType::Fade, FADE_IN_DELAY);
    get<tgui::Label>("GamePausedTitle")->showWithEffect(tgui::ShowAnimationType::Fade, FADE_IN_DELAY);
}

void GameGUI::hideReturnMMConfirmWidget()
{
    get<tgui::Grid>("ReturnToMainMenuConfirmMenu")->hide();
    get<tgui::Label>("ReturnToMainMenuConfirmTitle")->hide();
}

void GameGUI::showReturnMMConfirmWidget()
{
    get<tgui::Grid>("ReturnToMainMenuConfirmMenu")->show();
    get<tgui::Label>("ReturnToMainMenuConfirmTitle")->show();
}
